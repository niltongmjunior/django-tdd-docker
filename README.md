# Test-Driven Development with Django, Django REST Framework, and Docker

[![pipeline status](https://gitlab.com/niltongmjunior/django-tdd-docker/badges/main/pipeline.svg)](https://gitlab.com/niltongmjunior/django-tdd-docker/commits/main)

This repository contains the contents for the [__Test-Driven Development with Django, Django REST Framework, and Docker__](https://testdriven.io/courses/tdd-django/) course from testdriven.io

The course covers:

- Python
- Django
- Django REST Framework
- Docker
- Postgres
- Gunicorn
- Swagger/OpenAPI
- CoreAPI
- TDD (with pytest and Coverage.py)
- flake8
- black
- isort
- HTTPie
- CI/CD pipelines on Gitlab
- Heroku

This app is currently running on [Heroku (click me to ping!)](https://django-tdd-docker-nilton.herokuapp.com/ping)

API documentation can be seen [here (Swagger)](https://django-tdd-docker-nilton.herokuapp.com/swagger-docs/) or [here (CoreAPI)](https://django-tdd-docker-nilton.herokuapp.com/docs/)